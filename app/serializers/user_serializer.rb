class UserSerializer < ActiveModel::Serializer
  attributes :id, :email, :created_at, :updated_at, :auth_token, :product_ids
  
  # Workaround for active_model_serializers 0.10.x, `embed` method has been removed.
  def product_ids
    object.products.pluck(:id)
  end
  
end
